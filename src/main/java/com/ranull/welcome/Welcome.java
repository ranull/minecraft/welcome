package com.ranull.welcome;

import com.ranull.welcome.commands.WelcomeCommand;
import com.ranull.welcome.events.Events;
import com.ranull.welcome.welcome.WelcomeManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Welcome extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        WelcomeManager welcomeManager = new WelcomeManager(this);
        getServer().getPluginManager().registerEvents(new Events(this, welcomeManager), this);
        this.getCommand("welcome").setExecutor(new WelcomeCommand(this));
    }
}
