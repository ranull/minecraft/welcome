package com.ranull.welcome.events;

import com.ranull.welcome.Welcome;
import com.ranull.welcome.welcome.NewPlayer;
import com.ranull.welcome.welcome.WelcomeManager;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerCommandEvent;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

public class Events implements Listener {
    private Welcome plugin;
    private WelcomeManager welcomeManager;

    public Events(Welcome plugin, WelcomeManager welcomeManager) {
        this.plugin = plugin;
        this.welcomeManager = welcomeManager;
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (welcomeManager.messageContains(event.getMessage())) {
            Player player = event.getPlayer();

            for (ConcurrentMap.Entry<Player, NewPlayer> entry : welcomeManager.getNewPlayers().entrySet()) {
                NewPlayer newPlayer = entry.getValue();
                int timeoutTime = plugin.getConfig().getInt("settings.timeoutTime") * 1000;

                if ((System.currentTimeMillis() - newPlayer.getJoinTime()) > timeoutTime) {
                    welcomeManager.removeNew(newPlayer.getPlayer());
                    continue;
                }

                if (!newPlayer.getPlayer().equals(player)) {
                    if (!newPlayer.hasPlayer(player)) {
                        newPlayer.addWelcomePlayer(player);

                        List<String> rewardCommands = plugin.getConfig().getStringList("settings.rewardCommands");
                        ConsoleCommandSender consoleCommandSender = plugin.getServer().getConsoleSender();

                        for (String command : rewardCommands) {
                            command = command.replace("$player", player.getName())
                                    .replace("$new", newPlayer.getPlayer().getName())
                                    .replace("&", "§");

                            ServerCommandEvent commandEvent = new ServerCommandEvent(consoleCommandSender, command);
                            plugin.getServer().getPluginManager().callEvent(commandEvent);

                            if (!event.isCancelled()) {
                                plugin.getServer().getScheduler().callSyncMethod(plugin, () -> plugin.getServer().dispatchCommand(commandEvent.getSender(), commandEvent.getCommand()));
                            }
                        }

                        String rewardText = plugin.getConfig().getString("settings.rewardText")
                                .replace("$player", player.getName())
                                .replace("$new", newPlayer.getPlayer().getName())
                                .replace("&", "§");

                        if (!rewardText.equals("")) {
                            player.sendMessage(rewardText);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!event.getPlayer().hasPlayedBefore()) {
            welcomeManager.addNew(event.getPlayer());
            String newPlayerMessage = plugin.getConfig().getString("settings.newPlayerMessage")
                    .replace("$new", event.getPlayer().getName())
                    .replace("&", "§");
            if (!newPlayerMessage.equals("")) {
                plugin.getServer().broadcastMessage(newPlayerMessage);
            }
        }
    }
}
