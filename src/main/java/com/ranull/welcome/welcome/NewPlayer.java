package com.ranull.welcome.welcome;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class NewPlayer {
    private Long joinTime;
    private Player player;
    private List<Player> welcome = new ArrayList<>();

    public NewPlayer(Player player) {
        this.player = player;
        this.joinTime = System.currentTimeMillis();
    }

    public Player getPlayer() {
        return player;
    }

    public Long getJoinTime() {
        return joinTime;
    }

    public void addWelcomePlayer(Player player) {
        welcome.add(player);
    }

    public Boolean hasPlayer(Player player) {
        return welcome.contains(player);
    }
}
