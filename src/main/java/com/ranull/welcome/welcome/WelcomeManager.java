package com.ranull.welcome.welcome;

import com.ranull.welcome.Welcome;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class WelcomeManager {
    private Welcome plugin;
    private ConcurrentMap<Player, NewPlayer> newPlayers = new ConcurrentHashMap<>();

    public WelcomeManager(Welcome plugin) {
        this.plugin = plugin;
    }

    public ConcurrentMap<Player, NewPlayer> getNewPlayers() {
        return newPlayers;
    }

    public void addNew(Player player) {
        newPlayers.put(player, new NewPlayer(player));
    }

    public void removeNew(Player player) {
        newPlayers.remove(player);
    }

    public Boolean messageContains(String message) {
        List<String> welcomeText = plugin.getConfig().getStringList("settings.welcomeText");

        for (String string : welcomeText) {
            if (message.toLowerCase().contains(string.toLowerCase())) {
                return true;
            }
        }

        return false;
    }
}
